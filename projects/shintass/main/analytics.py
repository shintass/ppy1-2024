import pandas as pd
import numpy as np
from scipy import stats
import matplotlib.pyplot as plt
import seaborn as sns

class DataSet:
    def __init__(self, file_path: str):
        """
        Initializes the dataset from a CSV file.
        """
        self.data = pd.read_csv(file_path)
    
    def get_summary(self) -> pd.DataFrame:
        """
        Returns descriptive statistics of the dataset.
        """
        return self.data.describe()
    
    def clean_data(self) -> None:
        """
        Cleans the dataset by removing missing values and resetting the index.
        """
        self.data.dropna(inplace=True)
        self.data.reset_index(drop=True, inplace=True)
    
    def add_column(self, column_name: str, data: np.ndarray) -> None:
        """
        Adds a new column to the dataset.
        """
        self.data[column_name] = data
    
    def numpy_operations(self) -> tuple:
        """
        Performs basic NumPy operations (mean and standard deviation) on numerical columns.
        """
        numerical_data = self.data.select_dtypes(include=[np.number])
        array_data = numerical_data.values
        mean = np.mean(array_data, axis=0)
        std_dev = np.std(array_data, axis=0)
        return mean, std_dev
    
    def scipy_operations(self) -> np.ndarray:
        """
        Performs basic SciPy operations (z-scores) on numerical columns.
        """
        numerical_data = self.data.select_dtypes(include=[np.number])
        array_data = numerical_data.values
        z_scores = stats.zscore(array_data, axis=0)
        return z_scores

# usage with a real-world dataset
if __name__ == "__main__":
    iris = sns.load_dataset("iris")
    iris.to_csv('iris.csv', index=False)

    file_path = "iris.csv"
    dataset = DataSet(file_path)

    summary = dataset.get_summary()
    print("Summary Statistics:\n", summary)

    dataset.clean_data()

    new_column_data = np.random.randn(len(dataset.data))
    dataset.add_column('Random', new_column_data)

    mean, std_dev = dataset.numpy_operations()
    print("Mean of numerical data:\n", mean)
    print("Standard deviation of numerical data:\n", std_dev)

    z_scores = dataset.scipy_operations()
    print("Z-scores of numerical data:\n", z_scores)
