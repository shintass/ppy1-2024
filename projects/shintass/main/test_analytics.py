import unittest
import pandas as pd
import numpy as np
from analytics import DataSet

class TestDataSet(unittest.TestCase):
    def setUp(self):
        data = {
            'A': [1, 4, 7],
            'B': [2, 5, 8],
            'C': [3, 6, 9]
        }
        self.df = pd.DataFrame(data)
        self.df.to_csv('test_data.csv', index=False)
        self.dataset = DataSet('test_data.csv')
    
    def test_get_summary(self):
        summary = self.dataset.get_summary()
        self.assertIsInstance(summary, pd.DataFrame)
    
    def test_clean_data(self):
        self.dataset.clean_data()
        self.assertFalse(self.dataset.data.isnull().values.any())
    
    def test_add_column(self):
        new_column_data = np.random.randn(len(self.dataset.data))
        self.dataset.add_column('D', new_column_data)
        self.assertIn('D', self.dataset.data.columns)
    
    def test_numpy_operations(self):
        mean, std_dev = self.dataset.numpy_operations()
        self.assertEqual(len(mean), 3)
        self.assertEqual(len(std_dev), 3)
    
    def test_scipy_operations(self):
        z_scores = self.dataset.scipy_operations()
        self.assertEqual(z_scores.shape, self.dataset.data.select_dtypes(include=[np.number]).values.shape)

if __name__ == '__main__':
    unittest.main()
