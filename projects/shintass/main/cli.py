import click
import numpy as np
from analytics import DataSet

@click.group()
def cli():
    """Command Line Interface for DataSet Operations"""
    pass

@cli.command()
@click.argument('file_path', type=click.Path(exists=True))
def summary(file_path):
    """Get summary statistics of the dataset"""
    dataset = DataSet(file_path)
    click.echo(dataset.get_summary())

@cli.command()
@click.argument('file_path', type=click.Path(exists=True))
def clean(file_path):
    """Clean the dataset by removing missing values"""
    dataset = DataSet(file_path)
    dataset.clean_data()
    dataset.data.to_csv(file_path, index=False)
    click.echo(f"Cleaned data saved to {file_path}")

@cli.command()
@click.argument('file_path', type=click.Path(exists=True))
@click.argument('column_name')
def add_random_column(file_path, column_name):
    """Add a new column with random values to the dataset"""
    dataset = DataSet(file_path)
    new_column_data = np.random.randn(len(dataset.data))
    dataset.add_column(column_name, new_column_data)
    dataset.data.to_csv(file_path, index=False)
    click.echo(f"New column '{column_name}' added to {file_path}")

@cli.command()
@click.argument('file_path', type=click.Path(exists=True))
def numpy_ops(file_path):
    """Perform NumPy operations on numerical columns"""
    dataset = DataSet(file_path)
    mean, std_dev = dataset.numpy_operations()
    click.echo(f"Mean of numerical data:\n{mean}")
    click.echo(f"Standard deviation of numerical data:\n{std_dev}")

@cli.command()
@click.argument('file_path', type=click.Path(exists=True))
def scipy_ops(file_path):
    """Perform SciPy operations (z-scores) on numerical columns"""
    dataset = DataSet(file_path)
    z_scores = dataset.scipy_operations()
    click.echo(f"Z-scores of numerical data:\n{z_scores}")

if __name__ == "__main__":
    cli()
