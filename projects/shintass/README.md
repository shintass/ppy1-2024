This project is a Python-based data analysis tool designed to facilitate the exploration and analysis of datasets. It offers a set of features to perform common data operations such as generating summary statistics, cleaning data, adding columns, and conducting statistical operations using NumPy and SciPy.

Features:
Summary Statistics: Generate descriptive statistics for the dataset, including count, mean, standard deviation, minimum, maximum, and quartiles.
Data Cleaning: Remove missing values from the dataset and reset the index.
Add Columns: Add new columns to the dataset with custom data.
NumPy Operations: Calculate mean and standard deviation of numerical columns using NumPy.
SciPy Operations: Compute z-scores for numerical columns using SciPy.

Requirements:
Python 3.x, pandas, numpy, scipy, seaborn (for loading sample datasets), click (for creating command-line interfaces)